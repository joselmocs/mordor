var gulp = require('gulp');
var less = require('gulp-less');
var watch = require('gulp-watch');
// var rename = require('gulp-rename');
// var cssmin = require('gulp-cssmin');


gulp.task('watch', function () {
    gulp.watch('./assets/src/less/**/*.less', ['less']);
});

gulp.task('less', function () {
    gulp.src('./assets/src/less/app.less')
        .pipe(less().on('error', function(err) {
            console.log(err);
        }))
        // .pipe(cssmin().on('error', function(err) {
        //     console.log(err);
        // }))
        // .pipe(rename({
        //     suffix: '.min'
        // }))
        .pipe(gulp.dest('./assets/dist/'));

});

gulp.task('default', ['less', 'watch']);
