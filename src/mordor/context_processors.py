from django.conf import settings


def config(request=None):
    context = {}
    context['locale'] = {
        'code': 'pt-br',
        'timezone': settings.TIME_ZONE
    }
    context['server'] = {
        'name': 'Mordor',
        'host': 'http://localhost:8000'
    }

    return {
        'config': context
    }
