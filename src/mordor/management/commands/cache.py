from django.core.management.base import BaseCommand
from django.core.cache import cache


class Command(BaseCommand):
    help = 'Clear the specified pattern from cache'

    def add_arguments(self, parser):
        parser.add_argument('--p', type=str)

    def handle(self, *args, **options):
        if options['p']:
            cache.delete_pattern(options['p'])
            self.stdout.write('Successfully cleared pattern "%s"' % options['p'])
