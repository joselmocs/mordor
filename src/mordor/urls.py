from django.conf.urls import url, include
from django.contrib import admin
from mordor.views import IndexView, FormsView


urlpatterns = [
    url(r'^$', IndexView.as_view(), name="index"),
    url(r'^forms$', FormsView.as_view(), name="forms"),
    url(r'^account/', include('account.urls')),
    url(r'^admin/', admin.site.urls),
]
