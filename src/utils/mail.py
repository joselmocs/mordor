
def send_mail(subject, to, template, context={}, fail_silently=False):
    from django.conf import settings
    from django.core.mail import send_mail
    from django.template.loader import get_template
    from mordor.context_processors import config

    context.update(config())

    return send_mail(
        subject,
        get_template(template).render(context),
        settings.DEFAULT_FROM_EMAIL,
        to,
        fail_silently=fail_silently,
        html_message=get_template(template).render(context)
    )
