from django.conf import settings
from django.contrib import auth
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, JsonResponse
from django.views.generic import View, TemplateView
import ujson

from security.exceptions import AuthLockedOut, AuthUserLockedOut
from security.decorators import limit_attempts
from utils.mail import send_mail


class AccountLogin(View):
    http_method_names = ['post']

    @limit_attempts(wait=1)
    def post(self, request):
        response = {
            'success': False,
            'messages': []
        }
        data = ujson.loads(request.body.decode())

        try:
            auth_user = auth.authenticate(
                request=request,
                username=data['username'].upper().strip(),
                password=data['password'].strip()
            )
        except AuthLockedOut:
            response['messages'].append('Você está bloqueado por %s segundos por várias tentativas erradas de login.' % settings.AUTH_LOCKOUT_WINDOW)
            return JsonResponse(response)
        except AuthUserLockedOut:
            response['messages'].append('Conta bloqueada por excesso de tentativas inválidas de login.')
            response['messages'].append('Enviamos para seu email instruções para desbloqueio da conta.')
            response['messages'].append('Se não recebeu o email, clique em AJUDA no menu superior.')
            return JsonResponse(response)

        if auth_user:
            if auth_user.is_active:
                auth.login(request, auth_user)
                response['success'] = True
            else:
                response['messages'].append('Conta não ativada. Por favor cheque seu email para ativação da mesma.')
        else:
            response['messages'].append('Nome de usuário ou senha inválidos.')

        return JsonResponse(response)


class AccountLogout(View):
    http_method_names = ['get']

    def get(self, request):
        auth.logout(request)
        return HttpResponseRedirect(reverse('index'))


class AccountCreate(TemplateView):
    http_method_names = ['get', 'post']
    template_name = "account/create.html"

    def post(self, request):
        response = {
            'success': False,
            'messages': []
        }
        data = ujson.loads(request.body.decode())

        username = data['username'].strip().upper()
        email = data['email'].strip().upper()

        if User.objects.filter(username=username).count():
            response['messages'].append('Este nome de usuário já foi escolhido. Por favor, escolha outro.')

        elif User.objects.filter(email=email).count():
            response['messages'].append('Este endereço de email já está cadastrado. Se você o usou, tente recuperar sua senha.')

        else:
            user = User()
            user.username = username
            user.email = email
            user.first_name = data['first_name'].strip().upper()
            user.last_name = data['last_name'].strip().upper()
            user.set_password(data['password'].strip())
            user.is_active = False
            user.save()
            user.userprofile.refresh_salt()

            send_mail(
                subject='Ativação de Conta',
                to=[user.email.lower()],
                template='account/mail/activation.html',
                context={
                    'user': user
                },
                fail_silently=False
            )
            response['success'] = True

        return JsonResponse(response)


class AccountUnlock(TemplateView):
    http_method_names = ['get']
    template_name = "account/unlock.html"

    def get(self, request, username, salt):
        response = {
            'success': False,
            'messages': []
        }

        user = None
        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            response['messages'].append('Link de desbloqueio inválido.')

        if user:
            if user.userprofile.auth_locked:
                if salt == user.userprofile.recovery_salt:
                    user.userprofile.auth_unlock()

                    response['success'] = True
                    response['messages'].append('Conta desbloqueada com sucesso! A partir de agora você já pode logar no site e no jogo usando esta conta.')
                else:
                    response['messages'].append('Link de desbloqueio inválido.')
            else:
                response['messages'].append('Esta conta já está desbloqueada. Você já pode logar no site e no jogo usando esta conta.')
                response['success'] = True

        return self.render_to_response(response)


class AccountRecover(TemplateView):
    http_method_names = ['get', 'post']
    template_name = 'account/recover.html'

    def post(self, request):
        response = {
            'success': False,
            'messages': []
        }

        data = json.loads(request.body.decode())

        try:
            user = User.objects.get(username=data['field_value'].strip())
        except User.DoesNotExist:
            user = None

        if data['type'] == "password":
            if user and not user.is_active:
                response['messages'].append('ACCOUNT_NOT_VERIFIED')
                return JsonResponse(response)
            elif not user:
                response['messages'].append('ACCOUNT_RETRIEVE_USERNAME_NOT_'
                                            'EXIST')
                return JsonResponse(response)

            # generate a new salt
            user.userprofile.generate_new_salt()

            response['success'] = True
            response['messages'].append('ACCOUNT_RETRIEVE_PASSWORD_SUCCESS')

            send_mail(
                subject='Recuperação de Senha',
                to=[user.email.lower()],
                template='account/mail/retrieve_password.html',
                context={
                    'user': user,
                    'host': 'http://' + request.get_host()
                }
            )

        elif data['type'] == "username":
            if user and not user.is_active:
                response['messages'].append('ACCOUNT_NOT_VERIFIED')
                return JsonResponse(response)
            elif not user:
                response['messages'].append('ACCOUNT_RETRIEVE_EMAIL_NOT_EXIST')
                return JsonResponse(response)

            response['success'] = True
            response['messages'].append('ACCOUNT_RETRIEVE_USERNAME_SUCCESS')

            send_mail(
                subject='Recuperação de nome de usuário',
                to=[user.email.lower()],
                template='account/mail/retrieve_username.html',
                context={
                    'user': user,
                    'host': 'http://' + request.get_host()
                }
            )

        elif data['type'] == "activation":
            if not user:
                response['messages'].append('ACCOUNT_RETRIEVE_EMAIL_NOT_EXIST')
                return JsonResponse(response)

            response['success'] = True
            if user.is_active:
                response['messages'].append('ACCOUNT_RETRIEVE_ACTIVATION_'
                                            'ALREADY_ACTIVE')
            else:
                response['messages'].append('ACCOUNT_ACTIVATION_MAIL_'
                                            'VERIFICATION')

                user.userprofile.generate_new_salt()
                send_mail(
                    subject='Ativação de Conta',
                    to=[user.email.lower()],
                    template='account/mail/activation.html',
                    context={
                        'user': user,
                        'host': 'http://' + request.get_host()
                    }
                )

        return JsonResponse(response)
