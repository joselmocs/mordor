from django.conf.urls import url

from account.views import AccountLogin, AccountLogout, AccountCreate, AccountUnlock, AccountRecover
from security.decorators import guest_required, login_required


urlpatterns = [
    # url(
    #     r'^$',
    #     login_required(Account.as_view()),
    #     name="account"
    # ),
    url(
        r'^login$',
        guest_required(AccountLogin.as_view()),
        name="account.login"
    ),
    url(
        r'^logout$',
        AccountLogout.as_view(),
        name="account.logout"
    ),
    url(
        r'^create$',
        guest_required(AccountCreate.as_view()),
        name="account.create"
    ),
    url(
        r'^unlock/(?P<username>.*)/(?P<salt>.*)$',
        guest_required(AccountUnlock.as_view()),
        name="account.unlock"
    ),
    # url(
    #     r'^activate/(?P<username>.*)/(?P<salt>.*)$',
    #     guest_required(AccountActivate.as_view()),
    #     name="account.activate"
    # ),
    url(
        r'^recover/$',
        guest_required(AccountRecover.as_view()),
        name="account.recover"
    ),
    # url(
    #     r'^retrieve/password/(?P<username>.*)/(?P<salt>.*)$',
    #     guest_required(AccountRetrievePassword.as_view()),
    #     name="account.retrieve.password"
    # ),
]
