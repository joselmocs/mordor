from django.contrib import admin
from django.utils.html import format_html

from account.models import UserProfile


class UserProfileAdmin(admin.ModelAdmin):
    model = UserProfile
    list_display = ('user', 'display_name', 'display_email', 'auth_locked', 'vote_points', 'game_coins', 'forum_posts_count', 'forum_replies_count')
    list_filter = ('user__is_active', 'auth_locked',)
    search_fields = ('user__username', 'user__email', 'user__first_name', 'user__last_name',)
    readonly_fields = ('display_username', 'recovery_salt',)
    ordering = ('user__pk',)
    actions = None

    def display_email(self, obj):
        return obj.user.email
    display_email.short_description = 'Email'
    display_email.admin_order_field = 'user__email'

    def display_name(self, obj):
        return obj.user.get_full_name()
    display_name.short_description = 'Name'
    display_name.admin_order_field = 'user__first_name'

    def display_username(self, obj):
        return format_html('<a href="../../../../auth/user/%s/change/" target="_blank">%s</a>' % (obj.user.pk, obj.user.username))
    display_username.short_description = 'User'

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    fieldsets = [
        (None, {
            'fields': ['display_username', 'recovery_salt', 'auth_locked']
        }),
        ('Premium', {
            'fields': ['vote_points', 'game_coins']
        }),
        ('Game', {
            'fields': ['active_character_id']
        }),
        ('Statistics', {
            'fields': ['forum_posts_count', 'forum_replies_count']
        }),
    ]


admin.site.register(UserProfile, UserProfileAdmin)
