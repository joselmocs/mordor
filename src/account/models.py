from django.db import models
from django.db.models.signals import post_save
from django.contrib.auth.models import User
from django.core.mail import send_mail


class UserProfile(models.Model):
    user = models.OneToOneField(User)
    auth_locked = models.BooleanField(
        'Auth locked',
        default=False,
        help_text="Locked by many invalid login attempts."
    )
    active_character_id = models.IntegerField('Active Character', default=0)
    vote_points = models.IntegerField('Vote Points', default=0)
    game_coins = models.IntegerField('Game Coins', default=0)
    forum_posts_count = models.IntegerField('Forum Posts', default=0)
    forum_replies_count = models.IntegerField('Forum Replies', default=0)
    recovery_salt = models.CharField(
        'Recovery Salt',
        max_length=62,
        blank=True,
        null=True,
        help_text="Used to activate/recover/unlock account."
    )

    class Meta:
        verbose_name = 'profile'

    def __str__(self):
        return "%s's profile" % self.user

    def refresh_salt(self, save=True):
        import uuid
        self.recovery_salt = str(uuid.uuid4())
        if save:
            self.save()

    def auth_unlock(self):
        self.auth_locked = False
        self.refresh_salt(save=False)
        self.save()

    def auth_lock(self, send_mail=False):
        from django.core.mail import send_mail
        from utils.mail import send_mail

        self.auth_locked = True
        self.refresh_salt(save=False)
        self.save()

        if send_mail:
            send_mail(
                subject='Bloqueio de Conta',
                to=[self.user.email.lower()],
                template='account/mail/account_locked.html',
                context={
                    'user': self.user
                },
                fail_silently=False
            )


def create_user_profile(sender, instance, created, **kwargs):
    if created:
        UserProfile.objects.create(user=instance)

post_save.connect(create_user_profile, sender=User)
