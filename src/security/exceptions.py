class AuthLockedOut(Exception):
    pass


class AuthUserLockedOut(Exception):
    pass
