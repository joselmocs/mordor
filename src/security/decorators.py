from django.conf import settings
from django.contrib.auth.models import User
from django.core.cache import cache
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponse
from functools import wraps

from security.exceptions import AuthLockedOut, AuthUserLockedOut


def guest_required(view_func):
    @wraps(view_func)
    def decorator(request, *args, **kwargs):
        if request.user.is_authenticated():
            return HttpResponseRedirect(reverse('index'))
        return view_func(request, *args, **kwargs)

    return decorator


def login_required(view_func):
    @wraps(view_func)
    def decorator(request, *args, **kwargs):
        if not request.user.is_authenticated():
            return HttpResponseRedirect(reverse('index'))
        return view_func(request, *args, **kwargs)

    return decorator


def limit_attempts(wait):
    def decorator(view_func, *args):
        @wraps(view_func, *args)
        def wrapper(*args, **kwargs):
            cache_key = "limit_attempts_%s_%s" % (args[1].client.unique_id, view_func.__qualname__)

            if cache.get(cache_key):
                cache.expire(cache_key, timeout=wait)
                if args[1].is_ajax():
                     return HttpResponse("", status=403)

            cache.set(cache_key, True)
            cache.expire(cache_key, timeout=wait)

            return view_func(*args, **kwargs)
        return wrapper
    return decorator


def auth_lockout(function):
    def wrapper(*args, **kwargs):
        try:
            user = User.objects.get(username=kwargs['username'])
        except User.DoesNotExist:
            user = None

        if user:
            if user.userprofile.auth_locked:
                raise AuthUserLockedOut()

            cache_key = "auth_attempts_%s" % kwargs['username']
            attempts = cache.get(cache_key) or 0
            if attempts >= settings.AUTH_USER_LOCKOUT_MAX_ATTEMPTS:
                user.userprofile.auth_lock(send_mail=True)
                cache.delete(cache_key)
                raise AuthUserLockedOut()

        else:
            cache_key = "auth_attempts_%s" % kwargs['request'].client.unique_id
            attempts = cache.get(cache_key) or 0
            if attempts >= settings.AUTH_LOCKOUT_MAX_ATTEMPTS:
                raise AuthLockedOut()

        result = function(*args, **kwargs)
        if result:
            cache.delete(cache_key)
        else:
            try:
                cache.set(cache_key, attempts + 1)
            except ValueError:
                cache.set(cache_key, 1)
            cache.expire(cache_key, settings.AUTH_LOCKOUT_WINDOW)

        return result

    return wraps(function)(wrapper)
