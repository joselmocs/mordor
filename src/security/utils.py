
class ClientId:
    _unique_id = None

    def __init__(self, ip, user_agent):
        self.ip = ip
        self.user_agent = user_agent

    @property
    def unique_id(self):
        from hashlib import md5

        if not self._unique_id:
            self._unique_id = md5(("%s;%s" % (self.ip, self.user_agent)).encode('utf-8')).hexdigest()
        return self._unique_id

    def __str__(self):
        return "%s/%s" % (self.ip, self.user_agent)
