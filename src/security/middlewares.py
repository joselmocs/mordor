from django.utils.deprecation import MiddlewareMixin

class ClientMiddleware(MiddlewareMixin):
    def process_request(self, request):
        from security.utils import ClientId

        client_ip = request.META.get('HTTP_X_FORWARDED_FOR', None)
        if client_ip:
            client_ip = client_ip.split(', ')[0]
        else:
            client_ip = request.META.get('REMOTE_ADDR', '')

        request.client = ClientId(
            client_ip,
            request.META.get('HTTP_USER_AGENT', '')
        )


class AuthLockoutMiddleware(MiddlewareMixin):
    def process_request(self, request):
        from django.contrib import auth
        from security.decorators import auth_lockout

        auth.authenticate = auth_lockout(auth.authenticate)
