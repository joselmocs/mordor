(function () {
    'use strict';

    // start app
    var app = angular.module('mordor', [
        'ngCookies'
    ]);

    app.constant("Config", _config);

    // configuration for django + angular and localization
    app.config([
        '$httpProvider', '$interpolateProvider', 'Config',
        function ($httpProvider, $interpolateProvider, Config) {
            $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
            $httpProvider.defaults.xsrfCookieName = 'csrftoken';
            $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';

            $interpolateProvider.startSymbol('{$');
            $interpolateProvider.endSymbol('$}');

            moment.locale(Config.locale.code);
        }
    ]);

    app.directive('onEnter', function () {
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if (event.which === 13) {
                    scope.$apply(function () {
                        scope.$eval(attrs.onEnter);
                    });
                    event.preventDefault();
                }
            });
        };
    });

    app.directive('focusOn', function() {
        return function(scope, elem, attr) {
            scope.$on('focusOn', function(e, name) {
                if (name === attr.focusOn || name === attr.ngModel) {
                    elem[0].focus();
                }
            });
        };
    });

    app.factory('$focus', function ($rootScope, $timeout) {
        return function (name) {
            $timeout(function () {
                $rootScope.$broadcast('focusOn', name);
            });
        };
    });

    app.directive('protected', function () {
        return {
            link: function (scope, element) {
                element.bind("cut copy paste contextmenu", function (e) {
                    e.preventDefault();
                });
            }
        };
    });

    app.directive('momentAgo', ['Config', function (Config) {
        return function (scope, element, attrs) {
            element.html(moment(attrs.momentAgo).tz(Config.locale.timezone).fromNow());
        };
    }]);

    app.directive('help', function () {
        return {
            link: function (scope, element, attrs) {
                var span = element.parent().find('span');
                element.bind('focus', function () {
                    span.html(attrs.help);
                }).bind('blur', function () {
                    span.empty();
                });
            }
        };
    });

})();
