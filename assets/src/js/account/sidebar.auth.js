(function () {
    'use strict';

    var app = angular.module('mordor');

    app.controller('SidebarAuthController', ['$scope', '$http', '$focus', function ($scope, $http, $focus) {
        $scope.errors = [];
        $scope.loading = false;
        $scope.form = {
            username: null,
            password: null
        };

        $scope.validate = function () {
            var errors = [];
            var focused = false;
            if (!$scope.form.username) {
                errors.push('O campo Nome de Usuário é obrigatório.');
                if (!focused) {
                    $focus('form.username'); focused = true;
                }
            }
            if (!$scope.form.password) {
                errors.push('O campo Senha é obrigatório.');
                if (!focused) {
                    $focus('form.password'); focused = true;
                }
            }
            return errors;
        };

        $scope.submit =  function () {
            if ($scope.loading) return;

            $scope.errors = [];
            var errors = $scope.validate();
            if (errors.length > 0) {
                $scope.errors = errors;
                return false;
            }

            $scope.loading = true;

            $http.post("/account/login", {
                username: $scope.form.username,
                password: $scope.form.password
            }).then(function (response) {
                if (response.data.success) {
                    window.location = "";
                }
                else {
                    $scope.errors = response.data.messages;
                    $scope.loading = false;
                    $focus('form.username');
                }
            }, function (response) {
                if (response.data !== "") {
                    $scope.errors.push('Falha ao conectar ao servidor.');
                }
                $focus('form.username');
                $scope.loading = false;
            });
        };
    }]);
})();
