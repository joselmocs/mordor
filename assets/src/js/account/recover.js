(function () {
    'use strict';

    var app = angular.module('mordor');

    app.controller('AccountRecoverController', ['$scope', '$http', function ($scope, $http) {
        $scope.errors = [];
        $scope.loading = false;
        $scope.form = {
            username: null,
            password: null
        };

    }]);
})();
