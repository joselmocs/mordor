(function () {
    "use strict";

    var app = angular.module('mordor');

    app.controller('AccountCreateController', ['$scope', '$http', function ($scope, $http) {
        $scope.errors = [];
        $scope.loading = false;
        $scope.completed = false;

        $scope.form = {
            username: "",
            first_name: "",
            last_name: "",
            email: "",
            email_re: "",
            password: "",
            password_re: ""
        };

        $scope.validate = function () {
            var errors = [];

            // Username
            if ($scope.form.username.length < 3 || $scope.form.username.length > 16) {
                errors.push('Nome de usuário deve conter de 3 a 16 caracteres.');
            }
            else {
                if (/^[a-z0-9_-]{3,16}$/.test($scope.form.username) == false) {
                    errors.push('Nome de usuário deve conter apenas letras e números.');
                }
            }

            // First name
            if ($scope.form.first_name.length < 2 || $scope.form.first_name.length > 32) {
                errors.push('Primeiro nome deve conter de 2 a 32 caracteres.');
            }
            else {
                if (/^\s*[a-zA-Z,\s]+\s*$/.test($scope.form.first_name) == false) {
                    errors.push('Primeiro nome preenchido incorretamente ou foi deixado em branco.');
                }
            }

            // Last name
            if ($scope.form.last_name.length < 2 || $scope.form.last_name.length > 32) {
                errors.push('Sobrenome deve conter de 2 a 32 caracteres.');
            }
            else {
                if (/^\s*[a-zA-Z,\s]+\s*$/.test($scope.form.last_name) == false) {
                    errors.push('Sobrenome preenchido incorretamente ou foi deixado em branco.');
                }
            }

            // e-mail
            if (/^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test($scope.form.email) == false) {
                errors.push('E-mail preenchido incorretamente ou foi deixado em branco.');
            }
            else {
                if ($scope.form.email.split('@')[0].length <= 2) {
                    errors.push('E-mail preenchido incorretamente ou foi deixado em branco.');
                }
                else {
                    if ($scope.form.email != $scope.form.email_re) {
                        errors.push('Os endereços de e-mail devem ser iguais.');
                    }
                }
            }

            // password
            if ($scope.form.password.length < 4 || $scope.form.password.length > 32) {
                errors.push('Senha deve conter de 4 a 32 caracteres.');
            }
            else {
                if ($scope.form.password != $scope.form.password_re) {
                    errors.push('Senhas devem ser iguais.');
                }
            }

            return errors;
        };

        $scope.submit = function () {
            $scope.errors = [];

            var errors = $scope.validate();
            if (errors.length > 0) {
                $scope.errors = errors;
                return false;
            }

            $scope.loading = true;

            $http.post("/account/create", {
                username: $scope.form.username,
                password: $scope.form.password,
                first_name: $scope.form.first_name,
                last_name: $scope.form.last_name,
                email: $scope.form.email
            }).then(function (response) {
                if (response.data.success) {
                    $scope.completed = true;
                }
                else {
                    $scope.errors = response.data.messages;
                }
            }, function () {
                $scope.errors.push('Falha ao conectar ao servidor.');
            }).then(function () {
                $scope.loading = false;
            });
        };

    }]);
})();
